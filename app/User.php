<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'level',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $cast = [
        'email_verified_at' => 'datetime',
    ];
}
