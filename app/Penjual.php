<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjual extends Model
{
    protected $table = 'penjuals';
    protected $fillable = [
        'Id_penjual', 'total', 'tgl_t'
    ];


    protected $primaryKey = 'Id_penjual';
    public $incrementing = false;
    public function barang_keluar()
    {
        return $this->hasMany(Barang_keluar::class, 'Id_penjual', 'Id_pen');
    }
}
