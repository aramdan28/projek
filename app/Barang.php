<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';
    protected $fillable = [
        'nama', 'harga', 'image', 'qty', 'satuan', 'Id_ktgr'
    ];

    protected $primaryKey = 'Id_barang';

    public function barang_keluar()
    {
        return $this->hasMany(Barang_keluar::class, 'Id_barang', 'Id_pen');
    }
}
