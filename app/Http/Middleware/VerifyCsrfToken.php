<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/Barangmasuk/data',
        '/Barangkeluar/data',
        '/Barangdata/data', '/Baranghapus/{id}',
        '/Userdata/data', '/Userhapus/{id}', 
        '/Kategoridata/data', '/Kategrihapus/{id}',        
        '/Supplierdata/data', '/Supplierhapus/{id}'
    ];
}
