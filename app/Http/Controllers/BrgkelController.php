<?php

namespace App\Http\Controllers;

use App\Barang_keluar;
use App\Barang;
use App\Penjual;
use DB;
use Illuminate\Http\Request;

class BrgkelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Join = DB::table('barang_keluars')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->get();
        $ByKat = DB::table('barangs')
            ->Join('barang_keluars', 'barang_keluars.Id_brg', '=', 'barangs.Id_barang')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->get();
        return view('Admin.Barangkeluar.databarangkeluar', compact('Join', 'ByKat'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(Request $request)
    {
        $orderBy = 'barang_keluars.Id_brgkeluar';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'barang_keluars.Id_brgkeluar';
                break;
            case "1":
                $orderBy = 'barangs.nama';
                break;
            case "2":
                $orderBy = 'barang_keluars.qtykel';
                break;
            case "3":
                $orderBy = 'barangs.satuan';
                break;
            case "4":
                $orderBy = 'barang_keluars.Id_pen';
                break;
            case "5":
                $orderBy = 'penjuals.tgl_t';
                break;
        }
        $data = Barang_keluar::select([
            'barang_keluars.*',
            'barangs.nama as nama',
            'barangs.satuan as satuan',
            'penjuals.tgl_t as tgl_t'
        ])
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->Join('penjuals', 'penjuals.Id_penjual', '=', 'barang_keluars.Id_pen');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(barang_keluars.Id_brgkeluar) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barang_keluars.qtykel) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.satuan) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barang_keluars.id_pen) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(penjuals.tgl_t) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }


        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1) $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }




    public function create()
    {
        $Barang = Barang::all();
        return view('Admin.Barangkeluar.add_barangkeluar', compact('Barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Barang $Id_barang)
    {
        $request->validate([
            'Id_brg' => ['required'],
            'qty' => ['required'],
            'qtykel' => ['required'],
            'tgl_t' => ['required']
        ]);

        $Id_barang = $request->Id_brg;

        $qtykel = $request->qtykel;
        $qty = $request->qty;

        if ($qtykel > $qty) {
            return redirect('BarangkeluarAdd')->with('wrong', 'Jumlah permintaan Penjualan/barang barang keluar melebihi stock');
        } else {
            $qtytotal = $qty - $qtykel;

            Barang::find($Id_barang)->update([
                'qty' => $qtytotal,
            ]);

            Barang_keluar::create([
                'Id_brg' => $request->Id_brg,
                'qtykel' => $request->qtykel,
                'tgl_t' => $request->tgl_t
            ]);

            return redirect('Barangkeluar')->with('success', 'Data berhasil di input');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang_keluar  $barang_keluar
     * @return \Illuminate\Http\Response
     */
    public function show(Barang_keluar $barang_keluar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang_keluar  $barang_keluar
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Barang_keluar = Barang_keluar::find($id);
        $Barang = Barang::all();
        $Join = DB::table('barang_keluars')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->where('Id_brgkeluar', $id)
            ->get();


        return view('Admin.Barangkeluar.editbarangkeluar', compact('Barang_keluar', 'Barang', 'Join'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang_keluar  $barang_keluar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Id_brg2' => ['required'],
            'qty' => ['required'],
            'qtykel1' => ['required'],
            'qtykel2' => ['required'],
            'tanggal' => ['required']
        ]);

        $Id_barang = $request->Id_brg2;
        $qtykel1 = $request->qtykel1;
        $qtykel2 = $request->qtykel2;
        $qty = $request->qty;


        if ($qtykel1 > $qtykel2) {
            $qtykel = $qtykel1 - $qtykel2;
            $qtytotal = $qty + $qtykel;
        } else if ($qtykel1 < $qtykel2) {
            $qtykel = $qtykel2 - $qtykel1;
            $qtytotal = $qty - $qtykel;
        } else {
            $qtytotal = $qty;
        }

        Barang::find($Id_barang)->update([
            'qty' => $qtytotal,
        ]);

        Barang_keluar::find($id)->update([
            'Id_brg' => $request->Id_brg2,
            'qtykel' => $request->qtykel2,
            'tgl_t' => $request->tgl_t
        ]);

        return redirect('Barangkeluar')->with('success', 'Data berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang_keluar  $barang_keluar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang_keluar::find($id)->delete();
        return redirect('Barangmasuk');
    }

    public function kasir(Request $request)
    {

        $key = $request->get('keyword');

        $Barang = Barang::orderBy('nama');

        if ($key != null) {
            $Barang = Barang::where("nama", "LIKE", "%" . $key . "%")->get();
        }

        return view('Admin.Barangkeluar.kasir', compact('Barang'));
    }
}
