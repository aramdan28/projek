<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Supplier = Supplier::all();
        return view('Admin.Supplier.datasupplier', compact('Supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(Request $request)
    {
        $orderBy = 'suppliers.Id_supplier';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'suppliers.Id_supplier';
                break;
            case "1":
                $orderBy = 'suppliers.Nama';
                break;
            case "2":
                $orderBy = 'suppliers.Alamat';
                break;            
            case "3":
                $orderBy = 'suppliers.No_tlp';
                break;            
        }
        $data = Supplier::select([
            'suppliers.*'
        ]);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(suppliers.Id_supplier) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(suppliers.Nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(suppliers.Alamat) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(suppliers.No_tlp) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }


        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1) $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function create()
    {
        return view('Admin.Supplier.add_supplier');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'Nama' => ['required'],
            'Alamat' => ['required'],
            'No_tlp' => ['required'],
        ]);

            Supplier::create([
                'Nama' => $request->Nama,
                'Alamat' => $request->Alamat,
                'No_tlp' => $request->No_tlp
            ]);

            return redirect('Supplier')->with('success', 'Data berhasil di input');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Supplier = Supplier::find($id);
        return view('Admin.Supplier.editsupplier', compact('Supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Nama' => ['required'],
            'Alamat' => ['required'],
            'No_tlp' => ['required'],
        ]);


            Supplier::find($id)->update([
                'Nama' => $request->Nama,
                'Alamat' => $request->Alamat,
                'No_tlp' => $request->No_tlp
            ]);

            return redirect('/Supplier')->with('success', 'Data berhasil di ubah');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::find($id)->delete();
        return redirect('Supplier');
    }
}
