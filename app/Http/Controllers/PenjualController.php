<?php

namespace App\Http\Controllers;

use App\Barang_masuk;
use App\Barang_keluar;
use App\Penjual;
use App\Barang;
use App\Supplier;
use DB;
use Illuminate\Http\Request;
use PDF;

class PenjualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Join = DB::table('penjuals')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'penjuals.Id_ktgr')
            ->Join('barangs', 'barangs.Id_barang', '=', 'penjuals.Id_brg')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'penjuals.Id_sup')
            ->get();
        return view('Admin.Penjualan.datapenjualan', compact('Join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create()
    {
        $Barang = Barang::all();
        $Supplier = Supplier::all();
        $Join = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->get();
        return view('Admin.Penjualan.add_penjualan', compact('Barang', 'Supplier', 'Join'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Barang_keluar $Id_brgkeluar)
    {
        // $request->validate([
        //     'Id_brg' => ['required'],
        //     'Id_brgkeluar' => ['required'],
        //     'Id_Penjual' => ['required'],
        //     'total' => ['required'],
        //     'qty' => ['required'],
        //     'qtykel' => ['required'],
        // ]);

        $Id_barang = $request->Id_brg;



        // if ($qtykel > $qty) {
        //     return redirect('Kasir')->with('wrong', 'Jumlah permintaan Penjualan/barang barang keluar melebihi stock');
        // } else {

        for ($i = 0; $i < count($request->id_produk); $i++) {
            $produk     = $request->id_produk;
            $qtykel     = $request->qtykel[$i];
            $qty        = $request->qty_lama[$i];
            $qtytotal   = $qty - $qtykel;
            Barang::find($produk[$i])->update([
                'qty' => $qtytotal,
            ]);
        }

        $Id_penjual = uniqid();

        Penjual::create([
            'Id_penjual'    => $Id_penjual,
            'total' => $request->total,
            'tgl_t' => $request->tgl_t
        ]);

        for ($j = 0; $j < count($request->id_produk); $j++) {
            Barang_keluar::create([
                'Id_brg'    => $request->id_produk[$j],
                'qtykel'    => $request->qtykel[$j],
                "Id_pen"    => $Id_penjual,
            ]);
        }

        return redirect('Barangkeluar')->with('success', 'Data berhasil di input');
        // }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function show(Penjual $penjual)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Penjual = Penjual::find($id);
        $Barang = Barang::all();
        $Supplier = Supplier::all();
        $JoinA = DB::table('penjuals')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'penjuals.Id_ktgr')
            ->Join('barangs', 'barangs.Id_barang', '=', 'penjuals.Id_brg')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'penjuals.Id_sup')
            ->where('penjuals.Id_penjual', $id)
            ->get();
        $JoinB = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->get();


        return view('Admin.Penjualan.editpenjualan', compact('Penjual', 'Barang', 'Supplier', 'JoinA', 'JoinB'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Id_ktgr' => ['required'],
            'Id_brg' => ['required'],
            'Id_sup' => ['required']
        ]);

        Penjual::find($id)->update($request->all());

        return redirect('Penjual')->with('success', 'Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Penjual::find($id)->delete();
        return redirect('Penjual')->with('success', 'Data berhasil di Hapus');;
    }
}
