<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use app\Barang;
use app\Supplier;
use app\User;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Utama');
    }

    public function admin()
    {
        $Supplier = DB::table('suppliers')->count();
        $Kategori = DB::table('kategoris')->count();
        $Barang = DB::table('barangs')
            ->select(DB::raw('sum(qty) AS total'))
            ->get();
        $Join = DB::table('barang_keluars')
            ->select('barangs.nama', 'barang_keluars.created_at', DB::raw('SUM(qtykel) AS qtykel'))
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->orderBy('qtykel', 'DESC')
            ->groupBy('barangs.nama', 'barang_keluars.created_at')
            ->limit(1)
            ->get();
        return view('Admin.UtamaAdmin', compact('Supplier', 'Kategori', 'Barang', 'Join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Pengguna.UserAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'level' => ['required', 'integer'],
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => $request->level
        ]);

        return redirect('Userdata')->with('success', 'Data berhasil di input');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $User = User::find($id);
        return view('Pengguna.User', compact('User'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User = User::find($id);
        return view('Pengguna.Useredit', compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
        ]);

        if ($request->level == "Admin") {
            $level = 1;


            User::find($id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        } else {
            $level = 2;


            User::find($id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        }
        return redirect('/UtamaAdmin')->with('success', 'Data berhasil Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::find($id)->delete();
        return redirect('Userdata');
    }

    public function user()
    {
        return view('pengguna.Userdata');
    }

    public function data(Request $request)
    {
        $orderBy = 'users.id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'users.id';
                break;
            case "1":
                $orderBy = 'users.name';
                break;
            case "2":
                $orderBy = 'users.email';
                break;
            case "3":
                $orderBy = 'users.level';
                break;
        }
        $data = User::select([
            'users.*'
        ]);
        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(users.id) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(users.name) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(users.email) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(users.level) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }
        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1) $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}
