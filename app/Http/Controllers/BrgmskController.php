<?php

namespace App\Http\Controllers;

use App\Barang_masuk;
use App\Barang;
use App\Supplier;
use App\Kategori;
use DB;
use Illuminate\Http\Request;

class BrgmskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Join = DB::table('barang_masuks')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_masuks.Id_brg')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->get();
        $ByKat = DB::table('barangs')
            ->Join('barang_masuks', 'barang_masuks.Id_brg', '=', 'barangs.Id_barang')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->get();
        $Kat = Kategori::all();

        return view('Admin.Barangmasuk.databarangmasuk', compact('Join', 'ByKat', 'Kat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(Request $request)
    {
        $orderBy = 'barang_masuks.Id_brgmasuk';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'barang_masuks.Id_brgmasuk';
                break;
            case "1":
                $orderBy = 'barangs.nama';
                break;
            case "2":
                $orderBy = 'suppliers.Nama';
                break;
            case "3":
                $orderBy = 'barang_masuks.qtymsk';
                break;
            case "3":
                $orderBy = 'barangs.satuan';
                break;
            case "4":
                $orderBy = 'barang_masuks.tanggal';
                break;
        }
        $data = Barang::select([
            'barangs.*',
            'barang_masuks.Id_brg as Id_brg',
            'barang_masuks.Id_brgmasuk as Id_brgmasuk',
            'barang_masuks.qtymsk as qtymsk',
            'barang_masuks.tanggal as tanggal',
            'suppliers.Nama as Nama'
        ])
            ->Join('barang_masuks', 'barang_masuks.Id_brg', '=', 'barangs.Id_barang')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(barang_masuks.Id_brgmasuk) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(suppliers.Nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barang_masuks.qtymsk) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.satuan) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barang_masuks.tanggal) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        // if ($request->input('Id_ktgr') != null) {
        //     $data = $data->where('Id_ktgr', $request->kategori);
        // }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1) $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function create()
    {
        $Barang = Barang::all();
        $Supplier = Supplier::all();
        return view('Admin.Barangmasuk.add_barangmasuk', compact('Barang', 'Supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Barang $Id_barang)
    {
        $request->validate([
            'Id_brg' => ['required'],
            'Id_sup' => ['required'],
            'qty' => ['required'],
            'qtymsk' => ['required'],
            'tanggal' => ['required']
        ]);

        $Id_barang = $request->Id_brg;

        $qtymsk = $request->qtymsk;
        $qty = $request->qty;
        $qtytotal = $qty + $qtymsk;

        Barang::find($Id_barang)->update([
            'qty' => $qtytotal,
        ]);

        Barang_masuk::create([
            'Id_brg' => $request->Id_brg,
            'Id_sup' => $request->Id_sup,
            'qtymsk' => $request->qtymsk,
            'tanggal' => $request->tanggal
        ]);

        return redirect('Barangmasuk')->with('success', 'Data berhasil di input');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang_masuk  $barang_masuk
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Barang_masuk $barang_masuk)
    {
        $request->validate([
            'Id_ktgr' => ['required'],
        ]);

        $kategori = $request->Id_ktr;

        $ByKat = DB::table('barangs')
            ->Join('barang_masuks', 'barang_masuks.Id_brg', '=', 'barangs.Id_barang')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->where('Id_ktgr', '=', $kategori)
            ->get();
        $Kat = Kategori::all();

        return view('Admin.Barangmasuk.databarangmasuk', compact('ByKat', 'Kat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang_masuk  $barang_masuk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Barang_masuk = Barang_masuk::find($id);
        $Barang = Barang::all();
        $Supplier = Supplier::all();
        $Join = DB::table('barang_masuks')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_masuks.Id_brg')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->where('Id_brgmasuk', $id)
            ->get();

        return view('Admin.Barangmasuk.editbarangmasuk', compact('Barang_masuk', 'Barang', 'Supplier', 'Join'));
    }

    public function editjum(Request $request)
    {
        dd($request);
        $will_update = $request->except(['image', '_token', '_method']);


        $qtymsk = $request->qtymsk;
        $qtymsk2 = $request->qtymsk2;
        $qty = $request->qty;


        if ($qtymsk > $qtymsk2) {
            $qtymsk = $qtymsk - $qtymsk2;
            $qtytotal = $qty - $qtymsk;
        } else if ($qtymsk < $qtymsk2) {
            $qtymsk = $qtymsk2 - $qtymsk;
            $qtytotal = $qty + $qtymsk;
        } else {
            $qtytotal = $qty;
        }

        Barang_masuk::where('Id_barangmsk', $request->input('Id_barangmsk'))
            ->update(
                $will_update,
                ['qty' => $qtytotal]
            );

        Barang::where('Id_brg', $request->input('Id_brg'))
            ->update(
                $will_update,
                ['qty' => $qtytotal]
            );

        return response()->json(true);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang_masuk  $barang_masuk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Id_brg2' => ['required'],
            'Id_sup' => ['required'],
            'qty' => ['required'],
            'qtymsk1' => ['required'],
            'qtymsk2' => ['required'],
            'tanggal' => ['required']
        ]);

        $Id_barang = $request->Id_brg2;
        $qtymsk1 = $request->qtymsk1;
        $qtymsk2 = $request->qtymsk2;
        $qty = $request->qty;


        if ($qtymsk1 > $qtymsk2) {
            $qtymsk = $qtymsk1 - $qtymsk2;
            $qtytotal = $qty - $qtymsk;
        } else if ($qtymsk1 < $qtymsk2) {
            $qtymsk = $qtymsk2 - $qtymsk1;
            $qtytotal = $qty + $qtymsk;
        } else {
            $qtytotal = $qty;
        }

        Barang::find($Id_barang)->update([
            'qty' => $qtytotal,
        ]);

        Barang_masuk::find($id)->update([
            'Id_brg' => $request->Id_brg2,
            'Id_sup' => $request->Id_sup,
            'qtymsk' => $request->qtymsk2,
            'tanggal' => $request->tanggal
        ]);

        return redirect('Barangmasuk')->with('success', 'Data berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang_masuk  $barang_masuk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang_masuk::find($id)->delete();
        return redirect('Barangmasuk');
    }
}
