<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Kategori;
use DB;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Kat = Kategori::orderby('namakat', 'asc')->get();
        $Join = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->orderby('barangs.Id_barang', 'asc')
            ->get();
        return view('Admin.Barang.databarang', compact('Join', 'Kat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(Request $request)
    {
        $orderBy = 'barangs.Id_barang';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'barangs.Id_barang';
                break;
            case "1":
                $orderBy = 'barangs.nama';
                break;
            case "2":
                $orderBy = 'barangs.harga';
                break;
                // case "3":
                //     $orderBy = 'barangs.image';
                //     break;
            case "3":
                $orderBy = 'barangs.qty';
                break;
            case "4":
                $orderBy = 'barangs.satuan';
                break;
            case "5":
                $orderBy = 'kategoris.namakat';
                break;
        }
        $data = Barang::select([
            'barangs.*',
            'kategoris.namakat as namakat'
        ])
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(barangs.Id_barang) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.harga) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    // ->orWhereRaw('LOWER(barangs.image) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.qty) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(barangs.satuan) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(kategoris.namakat) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('kategori') != null) {
            $data = $data->where('Id_ktgr', $request->kategori);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1) $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function create()
    {
        $Kategori = Kategori::all();
        return view('Admin.Barang.add_barang', compact('Kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'harga' => ['required'],
            'image' => ['mimes:jpg,jpeg,png,gif,svg'],
            'qty' => ['required'],
            'satuan' => ['required'],
            'Id_ktgr' => ['required']
        ]);
        $imgName = 'gambar kosong';

        if ($request->image) {
            $file = $request->file('image');
            $imgName = $file->getClientOriginalName() . '.' . time()
                . '.' . $file->getClientOriginalExtension();

            $request->image->move(public_path('image'), $imgName);
        }
        Barang::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'image' => $imgName,
            'qty' => $request->qty,
            'satuan' => $request->satuan,
            'Id_ktgr' => $request->Id_ktgr
        ]);

        return redirect('Barangdata')->with('success', 'Data berhasil di input');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Barang = Barang::find($id);
        $Kategori = Kategori::all();
        $Join = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->where('barangs.Id_barang', $id)
            ->select('barangs.Id_ktgr', 'kategoris.namakat')
            ->get();
        return view('Admin.Barang.editbarang', compact('Barang', 'Kategori', 'Join'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Barang = Barang::find($id);

        $request->validate([
            'nama' => ['required'],
            'harga' => ['required'],
            'image' => ['mimes:jpg,jpeg,png,gif,svg'],
            'qty' => ['required'],
            'satuan' => ['required'],
            'Id_ktgr' => ['required']
        ]);

        if ($request->file('image') == "") {

            Barang::find($id)->update([
                'nama' => $request->nama,
                'harga' => $request->harga,
                'image' => $Barang->image,
                'qty' => $request->qty,
                'satuan' => $request->satuan,
                'Id_ktgr' => $request->Id_ktgr
            ]);
        } else {

            $file = $request->file('image');
            $imgName = $file->getClientOriginalName() . '.' . time()
                . '.' . $file->getClientOriginalExtension();

            $request->image->move(public_path('image'), $imgName);

            Barang::find($id)->update([
                'nama' => $request->nama,
                'harga' => $request->harga,
                'image' => $imgName,
                'qty' => $request->qty,
                'satuan' => $request->satuan,
                'Id_ktgr' => $request->Id_ktgr
            ]);
        }

        return redirect('/Barangdata')->with('success', 'Data berhasil Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::find($id)->delete();
        return redirect('Barangdata');
    }
}
