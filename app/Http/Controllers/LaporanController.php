<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang_masuk;
use App\Barang_keluar;
use App\Penjual;
use App\Barang;
use App\Supplier;
use DB;
use PDF;

class LaporanController extends Controller
{
    public function index()
    {
        $Join3 = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->orderby('barangs.Id_barang', 'asc')
            ->get();
        return view('Admin.Laporan.LStock', compact('Join3'));
    }

    public function cetak_pdf()
    {
        $Join3 = DB::table('barangs')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->orderby('barangs.Id_barang', 'asc')
            ->get();
        $pdf = PDF::loadview('Admin.Laporan.LapStock', compact('Join3'))->setPaper('A4', 'Potrait');
        return $pdf->stream();
    }

    public function tgllpenjualan()
    {
        return view('Admin.Laporan.laporan');
    }

    public function lpenjualan($tgl)
    {
        $tgl = $tgl;

        $Join2 = DB::table('barang_keluars')
            ->where('penjuals.tgl_t', '=', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%d-%b-%Y") as tgl_t'), 'barang_keluars.Id_pen', 'penjuals.total', 'penjuals.tgl_t')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->Join('penjuals', 'penjuals.Id_penjual', '=', 'barang_keluars.Id_pen')
            ->orderBy('barang_keluars.Id_pen', 'ASC')
            ->groupBy('barang_keluars.Id_pen', 'penjuals.total', 'penjuals.tgl_t')
            ->get();
        $Join3 = DB::table('barang_keluars')
            ->where('penjuals.tgl_t', '=', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%d-%b-%Y") as tgl_t'), 'barang_keluars.Id_pen', 'barangs.*', 'barang_keluars.qtykel')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->Join('penjuals', 'penjuals.Id_penjual', '=', 'barang_keluars.Id_pen')
            ->orderBy('barang_keluars.Id_pen', 'ASC')
            ->get();
        $T = DB::table('penjuals')->where('tgl_t', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%d-%b-%Y") as tgl_t'), 'barang_keluars.total')
            ->sum('total');

        return view('Admin.Laporan.LPen', compact('Join2', 'Join3', 'T', 'tgl'));
    }

    public function cetak2_pdf($tgl)
    {
        $tgl = $tgl;

        $Join2 = DB::table('barang_keluars')
            ->where('penjuals.tgl_t', '=', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%Y-%c-%d") as tgl_t'), 'barang_keluars.Id_pen', 'penjuals.total')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->Join('penjuals', 'penjuals.Id_penjual', '=', 'barang_keluars.Id_pen')
            ->orderBy('barang_keluars.Id_pen', 'ASC')
            ->groupBy('barang_keluars.Id_pen', 'penjuals.total', 'tgl_t')
            ->get();

        $Join3 = DB::table('barang_keluars')
            ->where('penjuals.tgl_t', '=', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%Y-%c-%d") as tgl_t'), 'barang_keluars.Id_pen', 'barangs.nama', 'barangs.satuan', 'barangs.harga', 'barang_keluars.qtykel')
            ->Join('barangs', 'barangs.Id_barang', '=', 'barang_keluars.Id_brg')
            ->Join('penjuals', 'penjuals.Id_penjual', '=', 'barang_keluars.Id_pen')
            ->orderBy('barang_keluars.Id_pen', 'ASC')
            ->get();
        $T = DB::table('penjuals')->where('tgl_t', $tgl)
            ->select(DB::raw('DATE_FORMAT(penjuals.tgl_t, "%Y-%c-%d") as tgl_t'), 'barang_keluars.total')
            ->sum('total');

        $pdf = PDF::loadview('Admin.Laporan.LapPen', compact('Join2', 'Join3', 'T', 'tgl'))->setPaper('A4', 'Potrait');
        return $pdf->stream('report.pdf', array('Attachment' => 0));
    }

    public function lbm()
    {
        $Join = DB::table('barangs')
            ->Join('barang_masuks', 'barang_masuks.Id_brg', '=', 'barangs.Id_barang')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->orderBy('barang_masuks.Id_brgmasuk', 'asc')
            ->get();
        return view('Admin.Laporan.LBmsk', compact('Join'));
    }

    public function cetak3_pdf()
    {
        $Join = DB::table('barangs')
            ->Join('barang_masuks', 'barang_masuks.Id_brg', '=', 'barangs.Id_barang')
            ->Join('suppliers', 'suppliers.Id_supplier', '=', 'barang_masuks.Id_sup')
            ->Join('kategoris', 'kategoris.Id_kategori', '=', 'barangs.Id_ktgr')
            ->orderBy('barang_masuks.Id_brgmasuk', 'asc')
            ->get();

        $pdf = PDF::loadview('Admin.Laporan.LapBmsk', compact('Join'))->setPaper('A4', 'Potrait');
        return $pdf->stream();
    }
}
