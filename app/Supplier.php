<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'Nama', 'Alamat', 'No_tlp'
    ];

    protected $primaryKey = 'Id_supplier';
}
