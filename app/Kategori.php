<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $fillable = ['namakat'];
    protected $primaryKey = 'Id_kategori';
}
