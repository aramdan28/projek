<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang_masuk extends Model
{
    protected $fillable = [
        'Id_brgmasuk', 'qtymsk', 'tanggal', 'Id_brg', 'Id_sup'
    ];

    protected $primaryKey = 'Id_brgmasuk';
}
