<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang_keluar extends Model
{
    protected $table = "barang_keluars";
    protected $fillable = [
        'Id_brgkeluar', 'qtykel', 'Id_brg', 'Id_pen'
    ];

    protected $primaryKey = 'Id_brgkeluar';
    public function penjual()
    {
        return $this->hasMany(Penjual::class, 'Id_penjual', 'Id_pen');
    }
    public function barang()
    {
        return $this->hasMany(Barang::class, 'Id_barang', 'Id_brg');
    }
}
