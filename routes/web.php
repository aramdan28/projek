<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', function () {
    return view('auth.register');
});

Auth::routes();




Route::get('/login', function () {
    return view('pengguna.login');
})->name('login');

Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');



Route::group(['middleware' => ['auth', 'Checklevel:1']], function () {


    Route::get('/Userdata', 'AdminController@user');
    Route::any('/Userdata/data', 'AdminController@data');
    Route::get('/UserAdd', 'AdminController@create');
    Route::any('/Userhapus/{id}', 'AdminController@destroy');

    Route::get('/Kategoridata', 'KategoriController@index');
    Route::get('/Kategori', 'KategoriController@create');
    Route::get('/Kategori/{id}/edit', 'KategoriController@edit');
    Route::put('/Kategori/{id}', 'KategoriController@update');
    Route::any('/Kategoridata/data', 'KategoriController@data');
    Route::any('/Kategorihapus/{id}', 'KategoriController@destroy');

    Route::get('/Barangdata', 'BarangController@index');
    Route::any('/Barangdata/data', 'BarangController@data');
    Route::get('/Barang', 'BarangController@create');
    Route::get('/Barang/{id}/edit', 'BarangController@edit');
    Route::put('/Barang/{id}', 'BarangController@update');
    Route::any('/Baranghapus/{id}', 'BarangController@destroy');

    Route::get('/Supplier', 'SupplierController@index');
    Route::get('/SupplierAdd', 'SupplierController@create');
    Route::get('/Supplier/{id}/edit', 'SupplierController@edit');
    Route::put('/Supplier/{id}', 'SupplierController@update');
    Route::any('/Supplierdata/data', 'SupplierController@data');
    Route::any('/Supplierhapus/{id}', 'SupplierController@destroy');


    Route::get('/Barangmasuk', 'BrgmskController@index');
    Route::get('/BarangmasukAdd', 'BrgmskController@create');
    Route::get('/Barangmasuk/{id}/edit', 'BrgmskController@edit');
    Route::patch('/Barangmasukedit', 'BrgmskController@editjum');
    Route::put('/Barangmasuk/{id}', 'BrgmskController@update');
    Route::put('/Barangmasukhapus/{id}', 'BrgmskController@destroy');
    Route::any('/Barangmasuk/data', 'BrgmskController@data');

    Route::get('/Barangkeluar', 'BrgkelController@index');
    // Route::get('/BarangkeluarAdd', 'BrgkelController@create');
    Route::get('/Barangkeluar/{id}/edit', 'BrgkelController@edit');
    Route::put('/Barangkeluar/{id}', 'BrgkelController@update');
    Route::put('/Barangkeluarhapus/{id}', 'BrgkelController@destroy');
    Route::any('/Barangkeluar/data', 'BrgkelController@data');

    Route::get('/Kasir', 'BrgkelController@kasir');

    Route::get('/Penjual', 'BrgkelController@create');


    Route::group(['prefix' => 'data'], function () {
        Route::resource('Kategori', 'KategoriController');
        Route::resource('Barang', 'BarangController');
        Route::resource('Supplier', 'SupplierController');
        Route::resource('Admin', 'AdminController');
        Route::resource('user', 'UserController'::class);
        Route::resource('Penjual', 'PenjualController');
        Route::resource('Barangmasuk', 'BrgmskController');
        Route::resource('Barangkeluar', 'BrgkelController');
    });
});

Route::group(['middleware' => ['auth', 'Checklevel:1,2']], function () {

    Route::get('/Userdetail/{id}', 'AdminController@show');
    Route::get('/User/{id}/edit', 'AdminController@edit');
    Route::put('/User/{id}', 'AdminController@update');

    Route::get('/UtamaAdmin', 'AdminController@admin');

    Route::get('/Laporan', 'LaporanController@index');
    Route::get('/Laporan/cetak_pdf',  'LaporanController@cetak_pdf')->name('print');

    Route::get('/TglLaporanPen', 'LaporanController@tgllpenjualan');
    Route::get('/LaporanPen/{tgl}', 'LaporanController@lpenjualan');
    Route::get('/LaporanPencetak/{tgl}',  'LaporanController@cetak2_pdf')->name('print');

    Route::get('/Laporanbm', 'LaporanController@lbm');
    Route::get('/Laporanbm/cetak_pdf',  'LaporanController@cetak3_pdf')->name('print');

    Route::group(['prefix' => 'data'], function () {
        Route::resource('Kategori', 'KategoriController');
        Route::resource('Barang', 'BarangController');
        Route::resource('Supplier', 'SupplierController');
        Route::resource('Admin', 'AdminController');
        Route::resource('user', 'UserController'::class);
        Route::resource('Penjual', 'PenjualController');
        Route::resource('Barangmasuk', 'BrgmskController');
        Route::resource('Barangkeluar', 'BrgkelController');
    });
});




// Route::get('/artikel', 'Articlecontroller@index');
// Route::get('/artikel/1', 'Articlecontroller@show');

// Route::get('user/login', 'userController@getLogin')->name('admin.login');
// Route::post('user/login', 'userController@postLogin');

// Route::get('/Admin', 'AdminController');


// Route::get('/PenjualAdd', 'PenjualController@create');
// Route::get('/Penjual/{id}/edit', 'PenjualController@edit');
// Route::put('/Penjual/{id}', 'PenjualController@update');

// Route::get('user/login', 'UserController@getLogin')->name('user.login');
// Route::post('user/login', 'UserController@postLogin');


// Route::get('/home', 'HomeController@index')->name('home');
