@extends('layouts.master')

@section('content') 




    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Data Barang</li>
      </ol>
    </section>

    
    <section class="content">

        
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col col-md-4">
                                <a href="/Barang/" class="btn btn-info btn-sm">Tambah Barang</a>
                            </div>
                            <div col="col col-md-4">
                                <form   method="GET">
                                    @csrf
                                    <select  id="kategori" style="width: 250px" class="form-control filter ">
                                        <option hidden>--Tampilkan Berdasarkan Kategori--</option>
                                        <option value>Semua Kategori</option>
                                        @foreach ($Kat as $k)
                                        <option value="{{ $k->Id_kategori}}">{{ $k->namakat }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>            
                    <div class="table-responsive">
                            <div class="box-body">
                                <table id="myTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Harga</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Kategori</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                
                            </tfoot>
                    
                            </table>               
                            </div>
                    </div>                
                </div>
            </div>
        </div>

</section>

@endsection

@section('js')


<script type="text/javascript">
    let kategori = $("Id_ktgr").val();

    const table = $('#myTable').DataTable({
    "pageLength": 3,
    "lengthMenu": [[3, 10, 25, 50, 100, -1], [3, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Barangdata/data",
        type: "POST",
        data:function(d){
        d.kategori = kategori;
        return d
        }
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Id_barang;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.nama;
        }
      },
      {
        "targets": 2,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.harga;
        }
      },
      // {
      //   "targets": 3,
      //   "sortable":false,
      //   "render": function(data, type, row, meta){
      //       let image = `
      //       <img src="/image/${row.image }" width='100' height='100'>
      //       `;
      //     return image;
      //   }
      // },
      {
        "targets": 3,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.qty;
        }
      },
      {
        "targets": 4,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.satuan;
        }
      },
      {
        "targets": 5,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.namakat;
        }
      },
      {
        "targets": 6,
        "sortable":false,
        "render": function(data, type, row, meta){
            let tampilan = ` 
            <div class="row">
                <div class="col-sm-2">
                <a href="/Barang/${row.Id_barang}/edit" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i></a>
                </div>
                <div class="col-sm-2">
                <form action="{{url('')}}/Baranghapus/${row.Id_barang}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data ${row.nama}')"><i class="fa fas fa-trash"></i></botton>
                    </form>
                </div>
            </div>
                `;
          return tampilan;
        }
      }
        ]
    })

    $(".filter").on('change',function(){
        kategori = $("#kategori").val()
        table.ajax.reload(null,false)
    });

</script>

@endsection

