@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang
        <small>Tambah Data Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah Data Barang</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="{{url('data/Barang')}}" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col col-lg-2">
                  <label>Nama Barang</label>
                  <input type="text" name="nama" class="form-control">
                </div>
                <div class="col col-lg-2">
                  <label>Harga</label>
                  <input type="text" name="harga" class="form-control">
                </div>
                <div class="col col-lg-2">
                  <label>Qty</label>
                  <input type="text" name="qty" class="form-control">
                </div>
                <div class="col col-lg-2">
                  <label>Satuan</label>
                  <input type="text" name="satuan" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col col-lg-3">
                  <label for="image">Cari Gambar</label>
                  <input type="file" name="image" class="form-control-file">
                </div>
                <div class="col col-lg-2">
                <label for="Id_ktgr">Pilih Kategori:</label>
                <select name="Id_ktgr" id="Id_ktgr" class="form-control js-example-basic-multiple">
                    @foreach ($Kategori as $kat)
                    <option value="{{ $kat->Id_kategori }}">{{ $kat->namakat }}</option>
                    @endforeach
                </select>
                </div>
              </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangdata/" class="btn btn-info btn-sm">Kembali</a>
            </div>
        </div>

    </form>

    </section>
    
@endsection
