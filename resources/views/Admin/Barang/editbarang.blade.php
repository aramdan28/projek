@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang
        <small>Edit Data Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Data Barang</li>
      </ol>
    </section>

    
    <section class="content">
        
    <form method="POST" action="/Barang/{{$Barang->Id_barang}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col col-lg-2">
                        <label>Nama Barang</label>
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') ? old('nama') : $Barang->nama }}">
                    </div>
                    <div class="col col-lg-2">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control" value="{{ old('harga') ? old('harga') : $Barang->harga }}">
                    </div>
                    <div class="col col-lg-2">                    
                        <label>Qty</label>
                        <input type="text" name="qty" class="form-control" value="{{ old('qty') ? old('qty') : $Barang->qty }}">
                    </div>
                    <div class="col col-lg-2">                    
                        <label>Satuan</label>
                        <input type="text" name="satuan" class="form-control" value="{{ old('satuan') ? old('satuan') : $Barang->satuan }}">
                    </div>
                </div>
            </br>
                            @if ($Barang->image)
                            <label>{{ old('image') ? old('image') : $Barang->image }}</label>
                        </br>
                            <img src="/image/{{ $Barang->image }}" width='100' height='100'>
                            @else
                            <P>Gambar Belum dimasukan</P>
                            @endif
                            <label for="image">Cari Gambar</label>
                            <input type="file" name="image" class="form-control-file" alue="{{ old('image') ? old('image') : $Barang->image }}" >
                        
                    </br>
                            <label for="Id_ktgr">Kategori:</label>
                            <select name="Id_ktgr" id="Id_ktgr" style="width:200px" class="form-control">
                            @foreach ($Join as $join)
                            <option value="{{ $join->Id_ktgr }}">-- {{ $join->namakat }} --</option>
                            @endforeach
                            @foreach ($Kategori as $kat)
                            <option value="{{ $kat->Id_kategori }}">{{ $kat->namakat }}</option>
                            @endforeach
                            </select>
                    </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangdata/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

</section>
    
@endsection
