@extends('layouts.master')

@section('content') 




    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>

    
    <section class="content">

        
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </br>
                        <div class="row">
                            <div class="col col-md-4">
                              <a href="" onclick="this.href='/LaporanPencetak/'+ document.getElementById('tgl').value " target="_blank" class="btn btn-success btn-outline-info"><i class="fa fas fa-eye"></i> Lihat Laporan</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="divider"></div>            
                    <div class="table-responsive">
                            <div class="box-body">
                              <div class="input-group mb-3">  
                                <label>Pilih Tanggal</label>
                                <input type="date" class="form-control" name="tgl" id="tgl" />
                              </div>             
                            </div>
                    </div>                
                </div>
            </div>
        </div>

</section>

@endsection

@section('js')


<script type="text/javascript">

    const table = $('#myTable').DataTable({
    "pageLength": 5,
    "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Userdata/data",
        type: "POST",
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.id;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.name;
        }
      },
      {
        "targets": 2,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.email;
        }
      },
      {
        "targets": 3,
        "sortable":false,
        "render": function(data, type, row, meta){
          if(row.level == 1){
            return `Admin`
          }else{
            return `Pemilik`
          }
        }
      },
      {
        "targets": 4,
        "sortable":false,
        "render": function(data, type, row, meta){
            let tampilan = ` 
                <form action="{{url('')}}/Userhapus/${row.id}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data ${row.nama}')"><i class="fa fas fa-trash"></i></botton>
                    </form>`;
          return tampilan;
        }
      }
        ]
    });

</script>

@endsection

