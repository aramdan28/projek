@extends('layouts.master')

@section('content') 




    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Masuk
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Laporan Barang Masuk</li>
      </ol>
    </section>

    
    <section class="invoice">
        <a href="/Laporanbm/cetak_pdf" class="btn btn-success">Cetak Laporan</a>
        <h1 style="text-align: center">Laporan Barang Masuk</h1>
        <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
        <div style="margin: 50px; width: 595px; height: 842px;">
        <table  class="table table-bordered" >
            <thead>
            <tr>
                  <th>No</th>
                  <th>Nama Barang Masuk</th>
                  <th>Tanggal Masuk</th>
                  <th>Supplier</th>
                  <th>Qty Masuk</th>
                  <th>Satuan</th>
                  <th>Kategori</th>

            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach ($Join as $j)
            <tr>

                <td>
                    {{ $i++ }}
                </td>
                <td>{{ $j->nama }}</td>
                <td>{{ $j->tanggal }}</td>
                <td>{{ $j->Nama }}</td>
                <td>{{ $j->qtymsk }}</td>
                <td>{{ $j->satuan }}</td>
                <td>{{ $j->namakat }}</td>
                
            </tr>
            </tbody>
            @endforeach
            <tfoot>
                
            </tfoot>
            
        </table>
        </div>

    </section>
 



@endsection

