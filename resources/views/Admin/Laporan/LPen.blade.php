@extends('layouts.master')

@section('content') 




    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>

    
    <section class="invoice">
        <a href="" onclick="this.href='/LaporanPenCetak/'+ document.getElementById('{{ $tgl }}').value " target="_blank" class="btn btn-success btn-outline-info"><i class="fa fas fa-print"></i> Cetak Laporan</a>
                            
        <h1 style="text-align: center">Laporan Penjualan</h1>
        <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
        </br>
        <p>Data di Ambil Pada Tanggal : {{ $tgl }}</p>
        <div style="margin: 50px; width: 800px; height: 842px;">
        <table  class="table table-bordered" >
            <thead>
            <tr>
                  <th>No</th>
                  <th>Id Transaksi</th>            
                  <th>Tanggal Penjualan</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Qty</th>
                  <th>Harga Satuan</th>                  
                  <th>Total Harga Perbarang </th>

            </tr>
            </thead>
            <tbody align="center">
            <?php $i = 1; ?>
            @foreach ($Join2 as $join2)
            <tr>

                <td>
                    {{ $i++ }}
                </td>
                <td>{{ $join2->Id_pen }}</td>
                <td>{{ $join2->tgl_t }}</td>            
                  @foreach ($Join3 as $j)
                  

                @if ("{{ $j->Id_pen }}" == "{{ $join2->Id_pen }}")
                    
                <td>{{ $j->nama }}</td>
                <td>{{ $j->satuan }}</td>   
                <td>{{ $j->qtykel }}</td>  
                <td>{{ $j->harga }}</td>  
                <td><?php  $a= $j->qtykel; $b =$j->harga; $c = $a*$b ?> {{ $c }} </td> 
                <tr></tr> 
                <td colspan="3"></td>
                
                @endif

                @endforeach
                <td colspan="4"><b>Total Pertransaksi</b></td>
                <td><b>{{ $join2->total }}</b></td>
            </tr>
            @endforeach
            <tr>
              <td colspan="7"><b>Total Transaksi Perhari</b></td>
              <td><b>{{ $T }}</b></td>
            </tr>
            </tbody>
          
            <tfoot>
                
            </tfoot>
            
        </table>
        </div>

    </section>
 



@endsection

