<!DOCTYPE html>
<html lang="en">
  

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
</head>

<body>

  <section class="invoice">
    <div style="margin: 20px; width: 595px; height: 842px;">
      <h1 style="text-align: center">Laporan Penjualan</h1>
      <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
    </br>
    <p>Data di Ambil Pada Tanggal : {{ $tgl }}</p>
      <table  class="table table-bordered" border="0.5" style="margin: 20px; width: 600px;">
        <thead>
        <tr>
              <th>No</th>
              <th>Id Transaksi</th>            
              <th>Tanggal Penjualan</th>
              <th>Nama Barang</th>
              <th>Satuan</th>
              <th>Qty</th>
              <th>Harga Satuan</th>                  
              <th>Total Harga Perbarang </th>

        </tr>
        </thead>
        <tbody align="center">
        <?php $i = 1; ?>
        @foreach ($Join2 as $join2)
        <tr>

            <td>
                {{ $i++ }}
            </td>
            <td>{{ $join2->Id_pen }}</td>
            <td>{{ $join2->tgl_t }}</td>   
         
              @foreach ($Join3 as $j)
            @if ("{{ $j->Id_pen }}" == "{{ $join2->Id_pen }}")
          
            <td>{{ $j->nama }}</td>
            <td>{{ $j->satuan }}</td>   
            <td>{{ $j->qtykel }}</td>  
            <td>{{ $j->harga }}</td>  
            <td><?php  $a= $j->qtykel; $b =$j->harga; $c = $a*$b ?> {{ $c }} </td> 
          <tr>
            <td colspan="3" ></td>
          
            @endif

            @endforeach
            
            <td colspan="4"><b>Total Pertransaksi</b></td>
            <td><b>{{ $join2->total }}</b></td>
        </tr>
        @endforeach
        <tr>
          <td colspan="7"><b>Total Transaksi Perhari</b></td>
          <td><b>{{ $T }}</b></td>
        </tr>
        </tbody>
      
        <tfoot>
            
        </tfoot>
        
    </table>
    </div>

  </section>

</body>

</html>