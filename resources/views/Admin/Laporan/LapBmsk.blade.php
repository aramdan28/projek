<!DOCTYPE html>
<html lang="en">
  

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
</head>

<body>

  <section class="invoice">
    <div style="margin: 20px; width: 595px; height: 842px;">
      <h1 style="text-align: center">Laporan Barang Masuk</h1>
      <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
      <table class="table table-bordered" border="0.5" style="margin: 20px; width: 600px;">
        <thead>
          <tr>
                <th>No</th>
                <th>Nama Barang Masuk</th>
                <th>Tanggal Masuk</th>
                <th>Supplier</th>
                <th>Qty Masuk</th>
                <th>Satuan</th>
                <th></th>
                <th>Kategori</th>

          </tr>
          </thead>
          <tbody>
          <?php $i = 1; ?>
          @foreach ($Join as $j)
          <tr align="center">

              <td>
                  {{ $i++ }}
              </td>
              <td>{{ $j->nama }}</td>
              <td>{{ $j->tanggal }}</td>
              <td>{{ $j->Nama }}</td>
              <td>{{ $j->qtymsk }}</td>
              <td>{{ $j->satuan }}</td>
              <td>{{ $j->namakat }}</td>
              
          </tr>
          </tbody>
          @endforeach
          <tfoot>
              
          </tfoot>

      </table>
    </div>

  </section>

</body>

</html>