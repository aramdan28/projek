@extends('layouts.master')

@section('content') 




    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Stock
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>

    
    <section class="invoice">
        <a href="/Laporan/cetak_pdf" class="btn btn-success">Cetak Laporan</a> 
      <h1 style="text-align: center">Laporan Stock</h1>
      <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
        <div style="margin: 50px; width: 595px; height: 842px;">
        <table  class="table table-bordered" >
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Satuan</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Kategori</th>

            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach ($Join3 as $join3)
            <tr>

                <td>
                    {{ $i++ }}
                </td>
                <td>{{ $join3->nama }}</td>
                <td>{{ $join3->harga }}</td>
                <td>{{ $join3->qty }}</td>
                <td>{{ $join3->satuan }}</td>
                <td>{{ $join3->namakat }}</td>
                
            </tr>
            </tbody>
            @endforeach
            <tfoot>
                
            </tfoot>
            
        </table>
        </div>

    </section>
 



@endsection

