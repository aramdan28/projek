<!DOCTYPE html>
<html lang="en">
  

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
</head>

<body>

  <section class="invoice">
    <div style="margin: 20px; width: 595px; height: 842px;">
      <h1 style="text-align: center">Laporan Stock</h1>
      <p>Hari, Tanggal : {{ date('l, Y-m-d') }}</p>
      <table class="table table-bordered" border="0.5">
        <thead>
          <tr>
            <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Satuan</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Kategori</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          @foreach ($Join3 as $join3)
          <tr>

            <td>
              {{ $i++ }}
            </td>
            <td>{{ $join3->nama }}</td>
            <td>{{ $join3->harga }}</td>
            <td>{{ $join3->qty }}</td>
            <td>{{ $join3->satuan }}</td>            
            <td>{{ $join3->namakat }}</td>

          </tr>
        </tbody>
        @endforeach
        <tfoot>

        </tfoot>

      </table>
    </div>

  </section>

</body>

</html>