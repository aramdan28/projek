@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Masuk
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Barang Masuk</li>
      </ol>
    </section>

    
    <section class="content">

        @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    
    

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
            <div class="row">
                <div class="col col-md-4">
                    <a href="/BarangmasukAdd/" class="btn btn-info btn-sm">Tambah Barang</a>
                </div>
                <div col="col col-md-4">
                    {{-- <form   method="GET">
                        @csrf
                        <select name="Id_ktgr" id="Id_ktgr" style="width: 200px" class="form-control filter">
                            <option hidden>--Cari Dengan Kategori--</option>
                            @foreach ($Kat as $k)
                            <option value="{{ $k->Id_ktgr}}">{{ $k->namakat }}</option>
                            @endforeach
                        </select>
                    </form> --}}
                </div>
            </div>
            </div>
            <div class="divider"></div>            
                <div class="table-responsive">
                    <div class="box-body">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Barang Masuk</th>
                                    <th>Supplier</th>
                                    <th>Qty</th>
                                    <th>Satuan</th>
                                    <th>Tanggal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <?php $i = 1; ?>
                                @foreach ($ByKat as $join)
                                <tr>

                                    <td>
                                        {{ $i++ }}
                                    </td>
                                    <td>{{ $join->nama }}</td>
                                    <td>{{ $join->Nama }}</td>
                                    <td>{{ $join->qtymsk }}</td>
                                    <td>{{ $join->tanggal }}</td>
                                    <td>{{ $join->namakat }}</td>
                                    <td>
                                        <a href="/Barangmasuk/{{$join->Id_brgmasuk}}/edit" class="btn btn-sm-successss">edit</a>
                                        <form action="{{ url('data/Barangmasuk', ['id' => $join->Id_brgmasuk])}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger" value="delete" onclick="return confirm('Apakah anda yakin ingin menghapus data {{$join->nama}}')">Delete</botton>
                                        </form>
                                    </td>
                                </tr> --}}
                            </tbody>
                                {{-- @endforeach --}}
                            </table>                    
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    
</section>



    {{-- <div class="modal fade" id="modal-edit">
      <div class="modal-dialog modal-sm-6">
        <form method="post" id="form-edit" action="{{url('Barangmasukedit')}}" enctype="multipart/form-data" class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Jumlah Barang Masuk</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="edit-body">            
          {{csrf_field()}}
            <input type="hidden" name="Id_brgmasuk">
            {{ method_field('PATCH') }}
              <div class="row">
                  <div class="col-md-6">
                      <label>Nama Barang </label>
                      <input type="text"readonly name="nama" class="form-control" >
                      <input type="hidden" readonly name="Id_brg" class="form-control" >
                  </div>
                      <div class="col-md-6">
                      <label>Banyak yang Masuk</label>
                      <input type="number" readonly name="qtymsk" class="form-control">
                      </div>
                  </div>    
              <div class="row">
                <div class="col-md-6">
                    <label>Stok </label>
                    <input type="text"readonly name="qty" class="form-control" required>
                </div>
                    <div class="col-md-6">
                    <label>Tambah jumlah Masuk</label>
                    <input type="number" min="1" name="qtymsk2" class="form-control">
                    </div>
                </div>     
          <div class="modal-footer justify-content-between">
            <button type="submit" class="btn btn-success" data-dismiss="modal">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          </div>
          </form>
        </div>
      </div> --}}
      

    
@endsection
@section('js')


<script type="text/javascript">
    let list_barang = [];

    const table = $('#myTable').DataTable({
    "pageLength": 5,
    "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Barangmasuk/data",
        type: "POST",
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          list_barang[row.Id_brgmasuk] = row;
          return row.Id_brgmasuk;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.nama;
        }
      },
      {
        "targets": 2,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Nama;
        }
      },
      {
        "targets": 3,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.qtymsk;
        }
      },
      {
        "targets": 4,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.satuan;
        }
      },
      {
        "targets": 5,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.tanggal;
        }
      },
      {
        "targets": 6,
        "sortable":false,
        "render": function(data, type, row, meta){
            let tampilan = ` 
            <a href="/Barangmasuk/${row.Id_brgmasuk}/edit" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i></a>
            
            `;
          return tampilan;
        }
      }
        ]
    })

    function showDetailbarang(Id_brgmasuk) {
        const Barang = list_barang[Id_brgmasuk]
        jQuery.noConflict();
        $("#modal-edit").modal('show')
        // SET SEMUA KE DEFAULT
        $("#form-edit input:not([name='_token']):not([name='_method'])").val('')

        $("#form-edit [name='Id_brgmasuk']").val(Id_brgmasuk)
        $("#form-edit [name='Id_brg']").val(Barang.Id_brg)
        $("#form-edit [name='nama']").val(Barang.nama)
        $("#form-edit [name='qtymsk']").val(Barang.qtymsk)
        $("#form-edit [name='qty']").val(Barang.qty)
    }

    // jQuery.noConflict();
    // $("#form-edit").on('submit',function(e){
    // e.preventDefault()
    // jQuery.noConflict();
    // $("#form-edit").ajaxSubmit({
    //   success:function(res){
    //     if(res===true){
    //       alert("BERHASIL UPDATE JUMLAH")
    //       table.ajax.reload(null,false)
    //       $("#modal-edit").modal('hide')
    //       }
    //     }
    //   })
    // })


</script>

@endsection
