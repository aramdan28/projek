@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Masuk
        <small>Tambah Data Barang Masuk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah Barang Masuk</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="{{url('data/Barangmasuk')}}">
        @csrf
        <div class="card-body">
            <div class="form-group">

                <label for="Id_brg">Pilih Barang:</label></br>
                <select name="Id_brg" id="Id_brg" style="width:200px" class="form-control js-example-basic-multiple">
                    <option></option>
                    @foreach ($Barang as $brg)
                    <option data-barang="{{$brg->qty}}"
                        value="{{ $brg->Id_barang }}">{{ $brg->nama }}</option>
                    @endforeach
                </select>
                <input type="hidden" name="qty" readonly />
            </br>
                <label for="Id_sup">Pilih Supplier:</label></br>
                <select name="Id_sup" id="Id_sup" style="width:200px" class="form-control js-example-basic-multiple">
                    <option></option>
                    @foreach ($Supplier as $sup)
                    <option value="{{ $sup->Id_supplier}}">{{ $sup->Nama }}</option>
                    @endforeach
                </select>
                </br>
                <label>Qty</label>
                <input type="text" name="qtymsk" class="form-control" style="width: 200px">
                </br>
                <label>Tanggal Masuk</label>
                <input type="date" name="tanggal" class="form-control" style="width: 200px">
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangmasuk/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#Id_brg').on('change', function(){
  // ambil data dari elemen option yang dipilih
  
  jQuery.noConflict();
  const barang = $('#Id_brg option:selected').data('barang');
  console.log(barang);

  // tampilkan data ke element
  $('[name=qty]').val(barang);
  
});

</script>
    

</section>

@endsection