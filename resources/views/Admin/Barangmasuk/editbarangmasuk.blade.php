@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Masuk
        <small>Edit Data Barang Masuk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Barang Masuk</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="/Barangmasuk/{{$Barang_masuk->Id_brgmasuk}}">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">

                <label for="Id_brg">Pilih Barang:</label>
                <select name="Id_brg2" id="Id_brg" readonly class="form-control" style="width: 200px">
                    @foreach ($Join as $join)
                    <option value="{{ $join->Id_brgmasuk }}">-- {{ $join->nama }} --</option>
                    @endforeach
                </select>
                <input type="hidden" name="qty" readonly /> 
            </br>

                <label for="Id_sup">Pilih Supplier:</label></br>
                <select name="Id_sup" id="Id_sup" class="form-control js-example-basic-multiple" style="width: 200px">
                    @foreach ($Join as $join)
                    <option value="{{ $join->Id_brgmasuk }}">-- {{ $join->Nama }} --</option>
                    @endforeach
                    @foreach ($Supplier as $sup)
                    <option value="{{ $sup->Id_supplier}}">{{ $sup->Nama }}</option>
                    @endforeach
                </select>
                </br>

                <label>Qty</label>
                <input style="width: 200px"  type="text" name="qtymsk2" class="form-control" value="{{ old('qtymsk') ? old('qtymsk') : $Barang_masuk->qtymsk }}">
                @foreach ($Join as $join)
                <input class="form-control" style="width: 200px" readonly type="hidden" name="qtymsk1"  value="{{ $join->qtymsk}}">
                @endforeach
                <label>Stock</label> 
                @foreach ($Join as $join)
                <input style="width: 200px" readonly type="text" name="qty" class="form-control" value="{{ $join->qty}}">
                @endforeach

                </br>
                <label>Tanggal Masuk</label>
                <input style="width: 200px" type="date" name="tanggal" class="form-control" value="{{ old('tanggal') ? old('tanggal') : $Barang_masuk->tanggal }}">
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangmasuk/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#Id_brg').on('change', function(){
  // ambil data dari elemen option yang dipilih
  const barang = $('#Id_brg option:selected').data('barang');

  // tampilkan data ke element
  $('[name=qty]').val(barang);
  
});
</script>
    

</section>
    

@endsection