<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Data Penjualan</title>
</head>

<body>
    <a href="/PenjualAdd/" class="btn btn-info btn-sm">Tambah Penjualan</a>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Barang</th>
            <th>Kategori</th>
            <th>Supplier</th>
            <th>Image</th>
            <th >Action</th>
        </tr>
        <?php $i = 1; ?>
        @foreach ($Join as $join )
        <tr>

            <td>
                {{ $i++ }}
            </td>
            <td>{{ $join->nama }}</td>
            <td>{{ $join->namakat }}</td>
            <td>{{ $join->Nama }}</td>
            <td><img src="/image/{{ $join->image }}" width='100' height='100'></td>            
            <td>
                <a href="/Penjual/{{$join->Id_penjual}}/edit" class="btn btn">edit</a>
                <form action="{{ route('Penjual.destroy', $join->Id_penjual)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data {{$join->nama}}')">Delete</botton>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

      
</section>
    
</div>

@endsection