<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Tambah Penjualan</title>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

</head>

<body>
    <form method="POST" action="/Penjual/{{$Penjual->Id_penjual}}">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="Id_brg">Pilih Barang:</label>
                <select name="Id_brg" id="Id_brg">
                    @foreach ($JoinA as $joina)
                    <option data-barang="{{$joina->Id_ktgr}}" data-kat="{{$joina->namakat}}"
                    value="{{ $joina->Id_penjual }}">--{{ $joina->nama }}--</option>
                    @endforeach
                    @foreach ($JoinB as $joinb)
                    <option data-barang="{{$joinb->Id_ktgr}}" data-kat="{{$joinb->namakat}}"
                    value="{{ $joinb->Id_barang }}">{{ $joinb->nama }}</option>
                    @endforeach
                </select>
                
                <label for="Id_ktgr">Kategori</label>
                <input value="{{ $joina->Id_penjual }}" type="hidden" name="Id_ktgr" readonly />
                <input value="{{ $joina->namakat }}" type="text" name="namakat" readonly />
            
            </br>
                <label for="Id_sup">Pilih Supplier:</label>
                <select name="Id_sup" id="Id_sup">
                    @foreach ($JoinA as $joina)
                    <option value="{{ $joina->Id_penjual }}">--{{ $joina->Nama }}--</option>
                    @endforeach
                    @foreach ($Supplier as $sup)
                    <option value="{{ $sup->Id_supplier }}">{{ $sup->Nama }}</option>
                    @endforeach
                </select>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Penjual/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>
    </form>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#Id_brg').on('change', function(){
  // ambil data dari elemen option yang dipilih
  const barang = $('#Id_brg option:selected').data('barang');
  const namakat = $('#Id_brg option:selected').data('kat');

  // tampilkan data ke element
  $('[name=Id_ktgr]').val(barang);
  $('[name=namakat]').val(namakat);
  
});
</script>

  
</section>
    
</div>

@endsection