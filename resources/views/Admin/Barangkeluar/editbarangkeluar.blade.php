@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Keluar
        <small>Edit Data Barang Keluar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Barang Keluar</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="/Barangkeluar/{{$Barang_keluar->Id_brgkeluar}}">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">

                <label for="Id_brg">Pilih Barang:</label>
                <select name="Id_brg2" id="Id_brg" readonly>
                    @foreach ($Join as $join)
                    <option value="{{ $join->Id_brgkeluar }}">-- {{ $join->nama }} --</option>
                    @endforeach
                </select>
                <input type="hidden" name="qty" readonly /> 
                </br>

                

                <label>Qty</label>
                <input style="width: 200px" type="text" name="qtykel2" class="form-control" value="{{ old('qtykel') ? old('qtykel') : $Barang_keluar->qtykel }}">
                @foreach ($Join as $join)
                <input readonly type="hidden" name="qtykel1" class="form-control" value="{{ $join->qtykel}}">
                @endforeach
                <label>Stock</label> 
                @foreach ($Join as $join)
                <input style="width: 200px" readonly type="text" name="qty" class="form-control" value="{{ $join->qty}}">
                @endforeach

                </br>
                <label>Tanggal Keluar</label>
                <input style="width: 200px" type="date" name="tanggal" class="form-control" value="{{ old('tanggal') ? old('tanggal') : $Barang_keluar->tanggal }}">
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangkeluar/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#Id_brg').on('change', function(){
  // ambil data dari elemen option yang dipilih
  const barang = $('#Id_brg option:selected').data('barang');

  // tampilkan data ke element
  $('[name=qty]').val(barang);
  
});
</script>
    


</section>
    

@endsection