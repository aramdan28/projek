@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Keluar
        <small>Tambah Data Barang Keluar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah Barang Keluar</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="{{url('data/Barangkeluar')}}">
        @csrf
        <div class="card-body">
            <div class="form-group">

                <label for="Id_brg">Pilih Barang:</label>
                <select name="Id_brg" id="Id_brg" style="width: 200px" class="form-control">
                    <option></option>
                    @foreach ($Barang as $brg)
                    <option data-barang="{{$brg->qty}}"
                        value="{{ $brg->Id_barang }}">{{ $brg->nama }}</option>
                    @endforeach
                </select>
                <label>Stock Barang:</label>
                </br>
                <input class="form-control" style="width: 200px" type="text" name="qty" readonly />
                </br>
                
                <label>Qty</label>
                <input style="width: 200px" type="text" name="qtykel" class="form-control">
                @if ($message = Session::get('wrong'))
                  <div class="alert alert-success">
                <p>{{ $message }}</p>
                </div>
                @endif
                </br>
                
                <label>Tanggal Keluar</label>
                <input style="width: 200px" type="date" name="tanggal" class="form-control">
                
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Barangkeluar/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    $('#Id_brg').on('change', function(){
  // ambil data dari elemen option yang dipilih
  const barang = $('#Id_brg option:selected').data('barang');

  // tampilkan data ke element
  $('[name=qty]').val(barang);
  
});
</script>
    

</section>
@endsection