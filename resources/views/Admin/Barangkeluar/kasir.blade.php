@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kasir
      </h1>
      <ol class="breadcrumb">
        <li><a href="/Utama"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Kasir</li>
      </ol>
    </section>

    
    <section class="content">
        <div class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                @if(session('created'))
                    <div class="row">
                      <div class="col-12">
                        <div class="alert alert-success"> Transaction Success
                        </div>
                      </div>
                    </div> 
                @endif
              </div>
          </div>
        </div>
                <div class="content">
                  <div class="container-fluid lead">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="box box-dark">
                          <div class="box-body">
                            <div id="js-requests-messages"></div> 
                            @if($errors->any())
                              <div class="alert alert-danger"> 
                                  @foreach($errors->all() as $error) - {{ $error }} 
                                  <br> 
                                  @endforeach
                              </div> 
                              @endif
                              <form method="POST" action="{{url('data/Penjual')}}">
                                  <div class="box-header text-white">
                                  <h3 class="box-title text-right">Tanggal Transaksi  :</h3> <input class="form-control" type="date" name="tgl_t" id="tgl_t"/>
                                  </div>
                                  <div class="box-body table-responsive p-0 text-white" style="height: 250px;">
                                  <div class="form-group">
                                  <table border="1" id="tabelinput" class="table table-bordered table-hovered table-striped" >
                                  <thead>
                                  <tr>
                                  <th>Kode</th>
                                  <th>Nama Barang</th>
                                  <th>Jumlah</th>
                                  <th>Harga Satuan</th>
                                  <th>Hapus</th>
                                  </tr>
                                  </thead>
                                      <tbody id="bodyinput">

                                      </tbody>
                                  </table>
                                  </div>
                                  
                                </div>
                                <div class="box-footer form-group text-right">
                                  <h5>TOTAL Rp.
                                    <input id="cartTotal" readonly name="total" class="text-bold">
                                </h5>
                                </div> 
                                @csrf 
                                <button type="submit" class="btn btn-dark btn-block btn-lg">Simpan Transaksi 
                                </button> 
                                </div>
                      </form>
                        </div>
                      </div>

                      <div class="col-md-6">

                            <div class="box bg-dark text-white">
                            <div class="box-header text-white">
                              <div class="row">
                                <div class="col col-md-3">
                                  <h3 class="box-title">List Product</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="box-body table-responsive p-0 text-white" style="height: 300px;">
                            <table id='myTable' class="table table-bordered table-striped" bordered="1">
                            <thead>
                            <tr>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Tambah</th>
                        </tr>
                        </thead>
                            <tbody id="bodyBarang"> 
                                @foreach($Barang as $j)
                            <tr id="barang{{ $j->Id_barang }}">
                            <td>{{ $j->Id_barang }}</td>
                            <td>{{ $j->nama }}</td>
                            <td> <button 
                              data-id="{{ $j->Id_barang }}" 
                              data-product-name="{{ $j->nama }}"
                              data-product-qty="{{ $j->qty }}"
                              data-product-harga="{{ $j->harga }}"  
                              onclick="addToCart(this)" class="btn btn-success btn-sm" ><i class="fa fas fa-plus"></i></button>
                        </td>
                        </tr> 
                        @endforeach
                        </tbody>
                        </table>
                        </div>
                            <div class="box-footer"></div>
                    </div>
                  </div>
                  </div>
                  </div>
                </div> 



    </section>
    <script src="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"></script>
    
    <script src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

@endsection

@section('js')

<script>

  $(document).ready( function () {
        $('#myTable').DataTable();
    } );


  function addToCart(product) {
    var id = product.getAttribute("data-id");
    var productName = product.getAttribute("data-product-name");
    var productQty = product.getAttribute("data-product-qty");
    var productHarga = product.getAttribute("data-product-harga");
  
  
//     var tabel = document.getElementById("tabelinput");
    var tambah = document.getElementById(`barang${id}`);
      
$( "#bodyinput" ).append(`
      <tr id="input${id}">
        <td>${id}</td>
        <td><input type="hidden" name="id_produk[]" value="${id}">${productName}</td>
        <td>
          <input type="hidden" name="qty_lama[]" value="${productQty}">
          <input type="number" style="width:100px" min="1" onchange="hitungTotal(this, ${id})" class="qty" name="qtykel[]" id="qty${id}">
        </td>
        <td id="harga${id}" class="harga">${productHarga}</td>
        <td><button type="button" class="btn btn-danger btn-sm"
            onclick="removeToCart(this)" data-id="${id}" 
                              data-product-name="${productName}"
                              data-product-qty="${productQty}"
                              data-product-harga="${productHarga}" ><i class="fa fas fa-trash"></i></button></td>
      </tr>
  `);

  $(`#barang${id}`).remove();
  hitungTotal(id)
  }
  
  function getCellValues() {
      var table = document.getElementById('tabelinput');
      var arrayTrans = [];
      for (var r = 1, n = table.rows.length; r < n; r++) {
        var id = table.rows[r].cells[0].innerHTML;
        var name = table.rows[r].cells[1].innerHTML;
        var qty = table.rows[r].cells[2].innerHTML;
        var harga = table.rows[r].cells[3].innerHTML;
        alert(id+"|"+name+"|"+qty+"|"+harga);
          
      }
  }

  function removeToCart(product) {
    var id = product.getAttribute("data-id");
    var hapus = document.getElementById(`barang${id}`);
    var barang = document.getElementById(`input${id}`);
    var productName = product.getAttribute("data-product-name");
    var productQty = product.getAttribute("data-product-qty");
    var productHarga = product.getAttribute("data-product-harga");

    $( "#bodyBarang" ).append(`
      <tr id="barang${id}">
        <td>${id}</td>
        <td>${productName}</td>
        <td> <button 
                              data-id="${id}" 
                              data-product-name="${productName}"
                              data-product-qty="${productQty}"
                              data-product-harga="${productHarga}"  
                              onclick="addToCart(this)" class="btn btn-success btn-sm" ><i class="fa fas fa-plus"></i></button>
                        </td>
      </tr>
  `);
    $(`#input${id}`).remove();
    hitungTotal(id)

      }

    const hitungTotal = (barang, id) => {
      let harga = document.getElementsByClassName("harga");
      let qty   = $(".qty");
      let total = 0;
      for (let index = 0; index < harga.length; index++) {
        total += parseInt(harga[index].innerHTML) * qty[index].value;
      }
      $("#cartTotal").val(total);
      $(`#qty${id}`).val(barang.value);
    }
  </script>
    
@endsection