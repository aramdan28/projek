@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Barang Keluar
        <small>Tabel Data Barang Keluar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Barang Keluar</li>
      </ol>
    </section>

    
    <section class="content">


    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
          <div class="row">
            <div class="col col-md-4">
              <a href="/Kasir/" class="btn btn-info btn-sm">Kasir</a>
            </div>
            <div col="col col-md-4">
            </div>
        </div>
          </div>

          <div class="box-body">
              <table id="myTable" class="table table-bordered table-hover">
                  <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Id Penjualan</th>
                    <th>Tanggal Penjualan</th>
                </tr>
                          </thead>
                          <tbody>
                {{-- <?php $i = 1; ?>
                @foreach ($Join as $join)
                <tr>

                    <td>
                        {{ $i++ }}
                    </td>
                    <td>{{ $join->nama }}</td>
                    <td>{{ $join->qtykel }}</td>
                    <td>{{ $join->tgl_t }}</td>
                  </tr> --}}
                          </tbody>
                {{-- @endforeach --}}
            </table>
          </div>
          </div>
      </div>
    </div>

    
</section>
    
@endsection

@section('js')


<script type="text/javascript">

    const table = $('#myTable').DataTable({
    "pageLength": 5,
    "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Barangkeluar/data",
        type: "POST"
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Id_brgkeluar;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.nama;
        }
      },
      {
        "targets": 2,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.qtykel;
        }
      },
      {
        "targets": 3,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.satuan;
        }
      },
      {
        "targets": 4,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Id_pen;
        }
      },
      {
        "targets": 5,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          
          return moment(row.tgl_t).format("M-D-Y");
        }
      }
        ]
    })


</script>

@endsection