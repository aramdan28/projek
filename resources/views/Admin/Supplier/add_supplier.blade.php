@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data Supplier
        <small>Data Supplier</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah Data Supplier</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="{{url('data/Supplier')}}">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Nama Supplier</label>
                <input style="width: 200px" type="text" name="Nama" class="form-control">
                </br>

                <label>Alamat</label>
                <textarea style="width: 200px;height:50px" type="text" name="Alamat" class="form-control"></textarea>
                </br>

                <label>No Telepon</label>
                <input style="width: 200px" type="text" name="No_tlp" class="form-control">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Supplier/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

  
</section>
    
@endsection