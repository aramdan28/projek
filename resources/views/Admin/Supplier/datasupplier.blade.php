@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Supplier
        <small>Tabel Data Supplier</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Data Supplier</li>
      </ol>
    </section>

    
    <section class="content">

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
            <a href="/SupplierAdd/" class="btn btn-info btn-sm">Tambah Supplier</a>
            </div>

            <div class="box-body">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>      
                        <tr>
                            <th>No</th>
                            <th>Nama Supplier</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                <tbody>
                </tbody>
            </table>

</section>
    
@endsection

@section('js')


<script type="text/javascript">

    const table = $('#myTable').DataTable({
    "pageLength": 5,
    "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Supplierdata/data",
        type: "POST"
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Id_supplier;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.Nama;
        }
      },
      {
        "targets": 2,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.Alamat;
        }
      },
      {
        "targets": 3,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.No_tlp;
        }
      },
      {
        "targets": 4,
        "sortable":false,
        "render": function(data, type, row, meta){
            let tampilan = ` 
            <div class="row">
                <div class="col-sm-2">
                <a href="/Supplier/${row.Id_supplier}/edit" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i></a>
                </div>
                <div class="col-sm-2">
                <form action="{{url('')}}/Supplierhapus/${row.Id_supplier}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data ${row.Nama}')"><i class="fa fas fa-trash"></i></botton>
                    </form>
                </div>
            </div>
                `;
          return tampilan;
        }
      }
        ]
    })

</script>

@endsection