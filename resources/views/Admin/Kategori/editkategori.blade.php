@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kategori
        <small>Data Kategori</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Kategori</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="/Kategori/{{$Kategori->Id_kategori}}">
        @csrf
        @method('PUT')

        <div class="card-body">
            <div class="form-group">
                <label>Nama Kategori</label>
                <input style="width: 300px" type="text" name="namakat" class="form-control" id="namakat" value="{{ old('namakat') ? old('namakat') : $Kategori->namakat }}">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Kategoridata/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>


</section>
    
@endsection