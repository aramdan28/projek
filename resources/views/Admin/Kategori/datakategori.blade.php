@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kategori

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Kategori</li>
      </ol>
    </section>

    
    <section class="content">

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="/Kategori/" class="btn btn-info btn-sm">Tambah Kategori</a>
            </div>

            <div class="box-body">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                        {{-- <?php $i = 1; ?>
                        @foreach ($Kategori as $kat) --}}
                    </thead>
                    <tbody>
                        {{-- <tr>
                            <td>
                                {{ $i++ }}
                            </td>
                            <td>{{ $kat->namakat }}</td>
                            <td >
                                <div class="row">
                                    <div class="col-sm-2">
                                    <a href="/Kategori/{{$kat->Id_kategori}}/edit" class="btn btn-sm btn-success">edit</a>
                                    </div>
                                    <div class="col-sm-2">
                                    <form action="{{ route('Kategori.destroy', $kat->Id_kategori)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data {{$kat->namakat}}')">Delete</botton>
                                    </form>
                                    </div>
                                </div>
                            </td>
                        </tr> --}}
                    </tbody>
                {{-- @endforeach --}}
            </table>
            </div>
            </div>
        </div>
    </div>
    
    
</section>
    

@endsection

@section('js')


<script type="text/javascript">

    const table = $('#myTable').DataTable({
    "pageLength": 5,
    "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'semua']],
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "processing":true,
    "bServerSide": true,
    "ajax":{
        url: "{{url('')}}/Kategoridata/data",
        type: "POST"
        },
        columnDefs: [
            { target: '_all', visible: true },
      {
        "targets": 0,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){
          return row.Id_kategori;
        }
      },
      {
        "targets": 1,
        "class":"text-nowrap",
        "render": function(data, type, row, meta){ 
          return row.namakat;
        }
      },
      {
        "targets": 2,
        "sortable":false,
        "render": function(data, type, row, meta){
            let tampilan = ` 
            <div class="row">
                <div class="col-sm-2">
                <a href="/Kategori/${row.Id_kategori}/edit" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i></a>
                </div>
                <div class="col-sm-2">
                <form action="{{url('')}}/Kategorihapus/${row.Id_kategori}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data ${row.namakat}')"><i class="fa fas fa-trash"></i></botton>
                    </form>
                </div>
            </div>
                `;
          return tampilan;
        }
      }
        ]
    })

</script>

@endsection