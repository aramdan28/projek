@extends('layouts.master')

@section('content') 

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard {{auth()->user()->name}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/Utama"><i class="fa fa-dashboard"></i>Home</a></li>
      </ol>
    </section>

    
    <section class="content">
      <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
          @foreach ( $Barang as $brg)
            <h3>{{ $brg->total }}</h3>
          @endforeach
            <p>Jumlah Stok Barang</p>
          </div>
          <div class="icon">
            <i class="fa fas fa-archive"></i>
          </div>
          <a href="/Laporan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

    @if(auth()->user()->level == 1)
      
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              @foreach ( $Join as $join)
              <h3>{{ $join->qtykel }} {{ $join->nama }}</h3>
            @endforeach
              <p>Paling Banyak Terjual</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="/Barangkeluar" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">

              <h3>{{ $Supplier }}</h3>
            
              <p>Banyak Supplier  </p>
            </div>
            <div class="icon">
              <i class="fa fas fa-industry"></i>
            </div>
            <a href="/Supplier/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $Kategori }}</sup></h3>

              <p>Banyak Kategori</p>
            </div>
            <div class="icon">
              <i class="fa fas fa-list-ul"></i>
            </div>
            <a href="/Kategori" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      
      <div class="">
      <a href="/Kasir" class="btn btn-info btn-xl" style="width: 150px; font-size: 30px">
        <i class="fa fas fa-shopping-cart"></i>Kasir
      </a>
      </div>
      @endif

    </section>
    
@endsection
