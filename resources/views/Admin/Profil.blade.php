@extends('layouts.master')

@section('content') 


    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Supplier
        <small>edit Data Supplier</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Data Supplier</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="/Supplier/{{$Supplier->Id_supplier}}">
        @csrf
        @method('PUT')
        
        <div class="card-body">
            <div class="form-group">
                <label>Nama Supplier</label>
                <input type="text" name="Nama" class="form-control" value="{{ old('Nama') ? old('Nama') : $Supplier->Nama }}">
                </br>

                <label>Email</label>
                <input type="text" name="Email" class="form-control" value="{{ old('Email') ? old('Email') : $Supplier->Email }}">
                </br>

                <label>New Password</label>
                <input id="password" type="password" name="Password1" class="form-control">
                @if ($message = Session::get('wrong'))
                <div class="alert alert-danger">
                <p>{{ $message }}</p>
                </div>
                @endif
                </br>
                <label>Confirm Password</label>
                <input id="cornfirm-password" type="password" name="Password2" class="form-control">
                @if ($message = Session::get('wrong'))
                <div class="alert alert-danger">
                <p>{{ $message }}</p>
                </div>
                @endif
                </br>

                <label>Alamat</label>
                <input type="text" name="Alamat" class="form-control" value="{{ old('Alamat') ? old('Alamat') : $Supplier->Alamat }}">
                </br>

                <label>No Telepon</label>
                <input type="text" name="No_tlp" class="form-control" value="{{ old('No_tlp') ? old('No_tlp') : $Supplier->No_tlp }}">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/Supplier/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

  
</section>
    


@endsection