@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data User
        <small>Data User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah User</li>
      </ol>
    </section>

    
    <section class="content">

    <form method="POST" action="{{url('data/Admin')}}">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" class="form-control" style="width: 300px">
            </div>
            <div class="form-group">
                <label for="email">Email</label>

                    <input id="email" type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>

            <div class="form-group">
                <label for="password">{{ __('Password') }}</label>

                    <input id="password" type="password" style="width: 300px" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror               
            </div>

            <div class="form-group">
                <label for="password-confirm">{{ __('Confirm Password') }}</label>

                    <input id="password-confirm" type="password" style="width: 300px" class="form-control" name="password_confirmation" required autocomplete="new-password">
                
            </div>

            <div class="form-group{{ $errors->has('level') ? 'has-error' : null }}">
                <label for="level">{{ __('level') }}</label>

                    <select name="level" id="" style="width: 300px" class="form-control">
                        <option value="1">admin</option>
                        <option value="2">pemilik</option>
                    </select>

                    @error('level')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror               
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="/Userdata/" class="btn btn-info btn-sm">Kembali</a>
            </div>

        </div>

    </form>

</section>
    

@endsection