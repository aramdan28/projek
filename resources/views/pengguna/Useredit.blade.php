@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data User
        <small>Data User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Tambah User</li>
      </ol>
    </section>

    
    <section class="content">

        <form method="POST" action="/User/{{auth()->user()->id}}">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label>Nama</label>
                    <input value="{{auth()->user()->name}}" type="text" name="name" class="form-control" style="width: 300px">
                </div>
                <div class="form-group">
                    <label for="email">{{ __('Email') }}</label>
    
                        <input value="{{auth()->user()->email}}" id="email" type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="email"  required autocomplete="email">
    
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
    
                <div class="form-group">
                    <label for="password">{{ __('Password') }}Baru</label>
    
                        <input  id="password" type="password" style="width: 300px" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
    
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror               
                </div>
    
                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
    
                        <input  id="password-confirm" type="password" style="width: 300px" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    
                </div>
    
    
                    <div class="form-group">
                        <label >Level</label>
                        @if(auth()->user()->level=="1")
                            <input id="email" value="Admin" readonly type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="level" >
                            @endif
                            @if(auth()->user()->level=="2")
                            <input id="email" value="Pemilik"  readonly type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="level" >
                            @endif
                    </div>
              
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/Userdata/" class="btn btn-info btn-sm">Kembali</a>
                </div>
    
            </div>

    </form>

</section>
    

@endsection