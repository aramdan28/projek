@extends('layouts.master')

@section('content') 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data User
        <small>Data User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      </ol>
    </section>

    
    <section class="content">

        <form method="POST" action="/User/{{auth()->user()->id}}">
            @csrf
            @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label>Nama</label>
                <input value="{{auth()->user()->name}}" readonly type="text" name="name" class="form-control" style="width: 300px" >
            </div>
            <div class="form-group">
                <label >Email</label>

                    <input id="email" value="{{auth()->user()->email}}"  readonly type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

            </div>

            <div class="form-group">
                <label >Level</label>
                @if(auth()->user()->level=="1")
                    <input id="email" value="Admin"  readonly type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @endif
                    @if(auth()->user()->level=="2")
                    <input id="email" value="Pemilik"  readonly type="email" style="width: 300px" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @endif
            </div>
            <div class="card-footer">
                <a href="/User/{{auth()->user()->id}}/edit" class="btn btn-sm btn-success"><i class="fa fas fa-edit"></i>Edit</a>
            </div>

        </div>

    </form>

</section>
    

@endsection