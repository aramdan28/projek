<header class="main-header" >
    <!-- Logo -->
    <a href="/UtamaAdmin" class="logo" style="background-color: #BBD38B">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{ asset('image/icon.png') }}" class="user-image" alt="User Image"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <img src="{{ asset('image/icon.png') }}" class="user-image" alt="User Image">
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color: #BBD38B">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      

      <div class="row" style="margin-right: 0">
        <div class="col col-sm-9" align="center">
          <h3><b>INVENTORY ARMINA</b></h3>
        </div>
          <div class="col col-sm-2" >
      <div class="navbar-custom-menu">
        
          <ul class="nav navbar-nav">
            
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('image') }}/user.jpeg" class="user-image" alt="User Image">
              <span class="hidden-xs">{{auth()->user()->name}}</span>
            </a>
            <ul class="dropdown-menu" >
              <!-- User image -->
              <li class="user-header" style="background-color: #777D71;">
                <img src="{{ asset('image') }}/user.jpeg" class="img-circle" alt="User Image">

                <p>
                  {{auth()->user()->name}} - level {{auth()->user()->level}}
                  {{-- <small>Member since Nov. 2012</small> --}}
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/Userdetail/{{auth()->user()->id}}" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
          </div>
    </nav>

    

  </header>