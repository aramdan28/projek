<aside class="main-sidebar" style="background-color: #777D71; ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('image') }}/user.jpeg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <a href="/Userdetail/{{auth()->user()->id}}"><h3>{{auth()->user()->name}}</h3></a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" >
        <li class="header" style="background-color:#BBD38B;">Daftar Menu</li>

        <li>
          <a href="/UtamaAdmin" ><i class="fa fa-book"></i> <span>Dashboard</span></a>
        </li>
        @if(auth()->user()->level=="1")
        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-user"></i><span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Userdata"><i class="fa fa-circle-o"></i>Data User</a></li>
            <li><a href="/UserAdd"><i class="fa fa-circle-o"></i>Tambah Data User</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-archive"></i><span>Barang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Barangdata/"><i class="fa fa-circle-o"></i>Data Barang</a></li>
            <li><a href="/Barang/"><i class="fa fa-circle-o"></i>Tambah Data Barang</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-list-ul"></i> <span>Kategori</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Kategoridata/"><i class="fa fa-circle-o"></i>Data Kategori</a></li>
            <li><a href="/Kategori/"><i class="fa fa-circle-o"></i>Tambah Data Kategori</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-sign-in"></i> <span>Barang Masuk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Barangmasuk/"><i class="fa fa-circle-o"></i>Data Barang
              Masuk</a></li>
            <li><a href="/BarangmasukAdd/"><i class="fa fa-circle-o"></i>Tambah Data Barang
              Masuk</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-sign-out"></i> <span>Barang Keluar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Barangkeluar/"><i class="fa fa-circle-o"></i>Data Barang
              Keluar</a></li>
            <li><a href="/Kasir/"><i class="fa fa-circle-o"></i>Kasir</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-industry"></i> <span>Supplier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Supplier/"><i class="fa fa-circle-o"></i>Data Supplier</a></li>
            <li><a href="/SupplierAdd/"><i class="fa fa-circle-o"></i>Tambah Data Supplier</a></li>
          </ul>
        </li>

        @endif

        <li class="treeview">
          <a href="#">
            <i class="fa fas fa-file"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Laporanbm/"><i class="fa fa-circle-o"></i>Barang Masuk</a></li>
            <li><a href="/TglLaporanPen/"><i class="fa fa-circle-o"></i>Penjualan</a></li>
            <li><a href="/Laporan/"><i class="fa fa-circle-o"></i>Stock</a></li>
          </ul>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>