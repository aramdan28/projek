<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_keluars', function (Blueprint $table) {
            $table->id('Id_brgkeluar');
            $table->unsignedBigInteger('Id_brg');
            $table->integer('qtykel');
            $table->string('Id_pen', 20);
            $table->foreign('Id_pen')->references('Id_penjual')->on('penjuals');
            $table->foreign('Id_brg')->references('Id_barang')->on('barangs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_keluars');
    }
}
