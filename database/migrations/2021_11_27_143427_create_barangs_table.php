<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->id('Id_barang');
            $table->string('nama', 100);
            $table->integer('harga');
            $table->string('image', 100);
            $table->integer('qty');
            $table->string('satuan', 10);
            $table->unsignedBigInteger('Id_ktgr');
            $table->foreign('Id_ktgr')->references('Id_kategori')->on('kategoris');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
